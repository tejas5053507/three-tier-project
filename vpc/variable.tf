variable "vpc_cidr" {
    default = "192.167.16.0/20"
    }
variable "project" {
    default = "three-tier"
}
variable "env" {
    default = "dev"
}
variable "private_subnet_cidr" {
    default = "192.168.16.0/20"
}
variable "public_subnet_cidr" {
    default = "192.168.16.0/20"
}
variable "image_id" {
    default = "ami-0440d3b780d96b29d"
}
variable "key_pair" {
    default = "universal_key"
}
variable "instance_type" {
    default = "t2.micro"
}