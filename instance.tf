
#resource "aws_instance" "instance_1" {
    ami = var.image_id
    instance_type = var.instance_type
    key_name = var.key_pair
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "${var.project}-private-instance"
        env = var.env
    }
    subnet_id = aws_subnet.private_subnet.id
}

#resource "aws_instance" "instance_2" {
    ami = var.image_id
    instance_type = var.instance_type
    key_name = var.key_pair
    vpc_security_group_ids = [aws_security_group.my_sg.id]
    tags = {
        Name = "${var.project}-public-instance"
        env = var.env
    }
    subnet_id = aws_subnet.public_subnet.id
}