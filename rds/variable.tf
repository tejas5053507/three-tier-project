variable "identifier" {
    default = "mydb-rds"
}
variable "storage" {
    default = "5"
}
variable "engine" {
    default = "mariadb"
}
variable "engine_version" {
    default = "10.5"
}

variable "db_name" {
    default  = "three-tier-project"
}

 variable "username" {
    default = "admin"
 }

 variable "password" {
    default = 123456789
 }
variable "subnet_1_cidr" {
    default   = "172.31.48.0/20"
}

variable "subnet_2_cidr" {
    default   = "172.31.64.0/20"
}
variable "az_1" {
    default = "ca-central-1a"
}

variable "az_2" {
    default   = "ca-central-1b"
}
variable "vpc_id" {
    default = "vpc-0437bebecb6842dbc"
}
variable "cidr_blocks" {
    default = "0.0.0.0/0"
}
variable "sg_name" {
    default    = "three-tier-project-sg"
}
variable "instance_class" {
    default   = "db.t3.micro"
}
