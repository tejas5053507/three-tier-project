provider "aws" {
    region = "ca-central-1"
}

resource "aws_db_instance" "maraidb" {
    depends_on         = ["aws_security_group.default"]
    identifier          = var.identifier
    allocated_storage   = var.storage
    engine              = var.engine
    engine_version      = var.engine_version
    instance_class      = var.instance_class
 # name                = "three-tier-db"
    username            = var.username
    password            = var.password
    vpc_security_group_ids = ["${aws_security_group.default.id}"]
    db_subnet_group_name   = aws_db_subnet_group.mariadb.id
    skip_final_snapshot    = "true"
    publicly_accessible    = "true"

}
resource "aws_db_subnet_group" "mariadb" {
    name                  = "main_subnet_group1"
    description           = "subnet group for the db"
    subnet_ids            = ["${aws_subnet.subnet_1.id}", "${aws_subnet.subnet_2.id}"]
}
resource  "aws_subnet" "subnet_1" {
    vpc_id          = var.vpc_id
    cidr_block      = var.subnet_1_cidr
    availability_zone = var.az_1

    tags  = {
        Name  = "first_subnet"
    }
}

resource  "aws_subnet" "subnet_2" {
    vpc_id          = var.vpc_id
    cidr_block      = var.subnet_2_cidr
    availability_zone = var.az_2

    tags  = {
        Name  = "first_subnet"
    }
}
resource "aws_security_group" "default" {
    name            =  "main_rds_sg1"
    vpc_id          =  var.vpc_id

    ingress {
        from_port = 0
        to_port   = 35535
        protocol  = "TCP"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress  {
        from_port    = 0
        to_port       = 0
        protocol      = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
    tags = {
        Name = var.sg_name
    }
}
